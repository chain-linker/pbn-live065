---
layout: post
title: "류쉐이 리우쉐이 LiuXueyi 刘学义 프로필 필모그래피 영화 드라마 보기"
toc: true
---


## 중국배우 류학의 류학의 류쉐이 리우쉐이 LiuXueyi 刘学义 프로필 필모그래피 영화 드라마 보기

##### 중국배우 류학의 류쉐이 LiuXueYi 프로필 필모그래피 영화 희곡 작 보기

사진출처는 글머리 류학의 웨이보입니다. 번역에 의역, 오역 있을 복수 있으며 최종 추가되는 필모와 정보에 대해서는 댓글로 남기겠습니다.
 류학의 나이는 1990년 7월 6일 생으로 키는 183cm 중앙희극학원 출신입니다. 고향은 산동성 청도시 (칭다오)입니다.

 2021년 5월 안목 소속사는 인제 유명한 환서세기입니다. 소속배우로는 양쯔, 임가륜, 성의, 원빙연, 진준걸, 모자준, 한동, 장여희등이 있습니다. 이역봉도 과거에 환서세기 대표배우였었죠. 금의지하, 유리미인살, 고검기담, 청운지등등 글머리 환서세기 제작입니다.
류학의도 청운지를 통해서 대중에게 엄청 알려지게 되죠. 환서세기는 좀 전천후 남자배우들이 있는 듯 합니다. 이역봉이 나가고 나서요. 성의와 임가륜도 최근에 빵 뜨게 되었고 류학의도 요즈음 작품활동하는 거 보면 이마적 방영을 기다리고 있는 주연작들이 기대가 시각 더더욱 되네요.
 

##### ✔ 류학의 영화

- 2011년 9월 호접마주 (蝴蝶魔咒) 연강천 (Yan Jiang Tian 演江天) 역
- 2011년 10월 여인연심 (女人莲心) 룡진방 (龙振邦) 역
- 2016년 1월 빙미인 (冰美人) 장몽생 (张梦生) 역
- 2019년 1월 천계지천제전설(天乩之天帝传说) 천제 (天帝)역

##### ✔ 류학의 드라마

- 2014년 10월 초련여우구락부 (初恋女友俱乐部) 방호 (方浩)역
- 2015년 11월 대옥아전기 (大玉儿传奇) : 총 50부작의 경첨 주연의 정교 판타지 고장극으로 액철 (Ejei Khan 额哲)역
- 2016년 아문적순진년대 (我们的纯真年代) 도강 (Tu Qiang 涂强) 역
- 2016년 7월 청운지1(青云志) 소일재 (Xiao Yi Cai 萧逸才)역
- 2016년 12월 청운지2 青云志Ⅱ 소일재 (Xiao Yi Cai 萧逸才) 역
- 2017년 5월 용주전기무간도 (龙珠传奇之无间道) : 진준걸 양쯔 주연의 고장극으로 오응기 (Wu Ying Qi 吴应麒)역. 치아 희곡 왓챠에서 봤는데 초반에는 재밌다가 후반에... 음.. 기연히 이랬어야 했나 싶은 드라마입니다.
- 2018년 7월 천계지백사전설 (天乩之白蛇传说) : 양쯔 임가륜 주연의 판타지 선협 고장극. 총 61부작으로 모자준등도 나오죠. 처음에 양쯔가 변신전 뱀이 무지무지 귀여워서 꽂혀서 보다가 일체 보긴 봤지만 쩜쩜쩜... 참황/천제 (Zhan Huang 斩荒/天帝)역

 - 2019년 6월 노해잠사 진령신수 (怒海潜沙&秦岭神树)
: 해우신 (Xie Yu Chen 解雨臣) 기능 도묘필기 시리즈 허리 하나입니다. 개인적 취향으로 도묘필기 시리즈 좋아하는데 사해와 이익 작품을 한복판 봤군요.
도묘필기는 과연 듣기에는 어려운 말이 도시 무척 나와서 얕은 외국어능력치로는 이해하기 어렵더라구요. ㅠ.ㅠ 영자막도 하는 지의 자체가 전문용어 안쓰는 말이 무지무지 나오다 보니 이해가 안가서 한글자막없으면 진입이 힘들어서 안봤던 드라마입니다. 해우신으로 나왔었다니!!! 해우신 직 좋아하는데 말입니다.
- 2020년 5월 추선 임소장 (Lin Xiao Zhuang 林小庄)역

 - 2020년 8월 유리미인살 호진/백제 (Hao Chen 昊辰/白帝)역

##### ✔ 류학의 미방영 차기작
 (2021년 5월기준)

- 상관완아(Shangguan Wan Er 上官婉儿): 무삼사 (Wu San Si 武三思)역. 총 60부작의 고장극으로 실지 촬영은 2013~14년에 된 작품입니다 방영이 될까...? 싶네요.
- 천목위궤 (Tian Mu Wei Ji 天目危机) : 미스터리 스릴러로 총 12부작. 이수부 (Li Xiu Fu 李秀夫)역

 - 봉신지천계 (封神之天启) : 장예 이만 주연의 총 72부작의 고장극으로 텐센트 방영예정작. 은교 (Yin Jiao 殷郊)역
- 청락 (清落): 애련 고장극으로 달달할 듯 합니다. 야수독 (Ye Xiu Du 夜修独) 역. 사항 이조 희곡 소개글 쓰다가 의식의 흐름으로 류학의 프로필과 필모를 쓰고 있네요. 어서 올릴 생각입니다. 리뷰 프리뷰 중드와 기타등등TV 카테고리에서 봐주세요.

 - 천고결진 (千古玦尘) : 천계(Tian Qi 天启)역. 훨씬 궁금하신 [왓챠](https://huskyimbibe.tk/entertain/post-00002.html){:rel="nofollow"} 분은 아랫수 득 글을 참조해주시면 됩니다.

 2020.10.25 - [리뷰,프리뷰/중드와 기타등등TV] - 주동우 허개 류학의 뢰예 천고결진(千古玦尘 ancient love poetry) 인간 줄거리

 - 야색암용시 (夜色暗涌时)

  : 모영택 (Mo Ling Ze 莫灵泽) 역. 자세한 건 속뜻 이윤 글을 참조해주세요.

 2021.05.16 - [리뷰,프리뷰/중드와 기타등등TV] - 장여희 류학의 야색암용시 夜色暗涌时 인걸 출연진 유개 보기
 - 낙화시절우봉군 ( Luo Hua Shi Jie You Feng Jun 落花时节又逢君) : 원빙연과 같이 찍는다는 소리가 있는데요. 아시죠. 중드는 크랭크인 들어가고 확정되기 전까지는 모르는 겁니다.

 앞으로가 더욱 기대가 되는 배우 류학의 입니다. 차기작 중에는 왕재미와 주연을 맡은 연모 고장극 청락이 소유자 기대가 됩니다.
고장극이 세밀히 어울리는 배우이기도 하고 달달 할 거 같아서요. 앞서 말했다시피 이년 제품 포스팅하다가 급 꽂혀서 류학의 프로필까지 쓴 거거든요.

##### '리뷰,프리뷰 > 스타' 카테고리의 다른 글

### 태그

### 관련글

### 댓글0
